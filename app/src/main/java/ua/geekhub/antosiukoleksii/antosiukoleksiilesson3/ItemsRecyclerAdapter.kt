package ua.geekhub.antosiukoleksii.antosiukoleksiilesson3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_item.view.*

class ItemRecyclerAdapter : RecyclerView.Adapter<ItemRecyclerAdapter.ViewHolder>() {
    private val items = mutableListOf<Int>()

    fun setNewItems (items: List<Int>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun addItem(item: Int) {
        items.add(item)
        notifyItemInserted(items.size)
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int  = items.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
         holder.onBind(items[position])
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun onBind(item: Int) {
            view.txtName.text = item.toString()
        }
    }
}