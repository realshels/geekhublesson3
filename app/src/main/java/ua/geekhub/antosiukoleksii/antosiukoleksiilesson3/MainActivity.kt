package ua.geekhub.antosiukoleksii.antosiukoleksiilesson3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), AsyncPrime.AsyncPrimeCallBack {
    private val adapter = ItemRecyclerAdapter()
    private var async: AsyncPrime? = null

    override fun beforeStart() {
        adapter.clear()
    }

    override fun onProgress(item: Int) {
        adapter.addItem(item)
        txtLastCalculated.text = getString(R.string.txtLastCalculatedCaption) + " " + item.toString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listItems.layoutManager = LinearLayoutManager(this)
        listItems.adapter = adapter
    }

    fun onStartClick(view: View) {
        if (async == null) {
            async = AsyncPrime(this)
            async?.execute()
        }
    }

    fun onStopClick(view: View) {
        async?.cancel(false)
        async = null
    }
}
