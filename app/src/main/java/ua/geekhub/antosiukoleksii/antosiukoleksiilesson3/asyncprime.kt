package ua.geekhub.antosiukoleksii.antosiukoleksiilesson3

import android.os.AsyncTask
import java.lang.Math.sqrt

class AsyncPrime(private val callback: AsyncPrimeCallBack): AsyncTask<Int, Int, Int>() {
    interface AsyncPrimeCallBack {
        fun beforeStart()
        fun onProgress(item: Int)
    }

    private fun isPrimeNumber(number: Int): Boolean {
        if (number <= 1) {
            return false
        }
        for (i in 2..number / 2) {
            if (number % i == 0) {
                return false
            }
        }

        return true
    }

    override fun onPreExecute() {
        super.onPreExecute()
        callback.beforeStart()
    }

    override fun doInBackground(vararg params: Int?): Int {
        for (i in 2..1_000_000) {
            if (isCancelled) {
                break;
            }
            if (isPrimeNumber(i)) {
                publishProgress(i);
            }
        }
        return 0
    }

    override fun onProgressUpdate(vararg values: Int?) {
        super.onProgressUpdate(*values)
        callback.onProgress(values[0]!!)
    }



}